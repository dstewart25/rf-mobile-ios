//
//  MainViewController.swift
//  Related Faces
//
//  Created by Daniel Stewart on 9/21/18.
//  Copyright © 2018 Related Faces. All rights reserved.
//

import UIKit
import AWSMobileClient

class HomeViewController: UIViewController {
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var navigationBar: UINavigationItem!
    
    weak var collectionView: UICollectionView!
    weak var uploadLabel: UILabel!
    weak var uploadButton: UIButton!
    weak var searchButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.revealViewController() != nil {
            menuButton.target = revealViewController() // this is the button located on the UINavigation bar
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
            self.view.addGestureRecognizer((self.revealViewController()?.tapGestureRecognizer())!)
        }
        
        // Adding search button to navigation bar
        let searchButton = UIBarButtonItem(image: UIImage(named: "ic_menu_search"), style: .plain, target: nil, action: nil)
        searchButton.tintColor = .white
        navigationBar.rightBarButtonItem = searchButton
        self.searchButton = searchButton
        
        // Setting up upload label
        let uploadLabel = UILabel(frame: .zero)
        uploadLabel.translatesAutoresizingMaskIntoConstraints = false
        uploadLabel.text = "Upload"
        uploadLabel.textColor = UIViewController.hexStringToUIColor(hex: "#33439B")
        uploadLabel.font = UIFont(name: "Lato-Bold", size: 17)
        uploadLabel.textAlignment = .center
        self.view.addSubview(uploadLabel)
        NSLayoutConstraint.activate([
            uploadLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 8),
            uploadLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 8),
            uploadLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -8)
            ])
        self.uploadLabel = uploadLabel
        
        // Setting up upload button
        let uploadButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        uploadButton.translatesAutoresizingMaskIntoConstraints = false
        uploadButton.setImage(UIImage(named: "ic_menu_upload"), for: UIControl.State.normal)
        uploadButton.backgroundColor = UIViewController.hexStringToUIColor(hex: "15BEE6")
        uploadButton.showsTouchWhenHighlighted = true
        uploadButton.layer.cornerRadius = uploadButton.frame.width/2
        uploadButton.clipsToBounds = true
        uploadButton.imageView?.tintColor = .white
        self.view.addSubview(uploadButton)
        NSLayoutConstraint.activate([
            uploadButton.topAnchor.constraint(equalTo: self.uploadLabel.bottomAnchor, constant: 8),
            uploadButton.widthAnchor.constraint(equalToConstant: 60),
            uploadButton.heightAnchor.constraint(equalToConstant: 60),
            uploadButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
            ])
        uploadButton.addTarget(self, action: #selector(onTapUpload(_:)), for: .touchUpInside)
        self.uploadButton = uploadButton
        
        // Setting up the collection view
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: 90, height: 120)
        let collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: self.uploadButton.bottomAnchor, constant: 8),
            collectionView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            ])
        self.collectionView = collectionView
        
        self.collectionView.backgroundColor = .white
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        self.collectionView.register(MyCell.self, forCellWithReuseIdentifier: "MyCell")
        self.collectionView.register(InfoCell.self, forCellWithReuseIdentifier: "InfoCell")
        self.collectionView.register(TipCell.self, forCellWithReuseIdentifier: "TipCell")
    }
    
    @objc func onTapUpload(_ sender: Any) {
        let revealViewController:SWRevealViewController = self.revealViewController()
        
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Camera", bundle: nil)
        let desController = mainStoryboard.instantiateViewController(withIdentifier: "ScanPhotoViewController") as! ScanPhotoViewController
        let newFrontViewController = UINavigationController.init(rootViewController: desController)
        
        revealViewController.pushFrontViewController(newFrontViewController, animated: true)
    }
}

/*
 * case 0: top three boxes holding numbers of photos, matches, then faces
 * case 1: tip of the day
 * case 2: user uploaded photos
 */
extension HomeViewController: UICollectionViewDataSource {
    // Three sections for metrics, tip of the day, and user uploaded photos
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    // Number of items in each section, must change case 2 to be in line with database showing how many photos user has uploaded
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        case 1:
            return 1
        case 2:
            return 30
        default:
            return 0
        }
    }
    
    // Doing individual cell classes for each item
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InfoCell", for: indexPath) as! InfoCell
                cell.numberLabel.text = "120"
                cell.subtitleLabel.text = "Photos"
                NSLayoutConstraint.activate([
                    cell.numberLabel.topAnchor.constraint(equalTo: cell.contentView.topAnchor, constant: cell.contentView.frame.height/4),
                    cell.subtitleLabel.topAnchor.constraint(equalTo: cell.numberLabel.bottomAnchor)
                    ])
                return cell
            case 1:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InfoCell", for: indexPath) as! InfoCell
                cell.numberLabel.text = "24"
                cell.subtitleLabel.text = "Matches"
                NSLayoutConstraint.activate([
                    cell.numberLabel.topAnchor.constraint(equalTo: cell.contentView.topAnchor, constant: cell.contentView.frame.height/4),
                    cell.subtitleLabel.topAnchor.constraint(equalTo: cell.numberLabel.bottomAnchor)
                    ])
                return cell
            case 2:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InfoCell", for: indexPath) as! InfoCell
                cell.numberLabel.text = "1,896"
                cell.subtitleLabel.text = "Faces"
                NSLayoutConstraint.activate([
                    cell.numberLabel.topAnchor.constraint(equalTo: cell.contentView.topAnchor, constant: cell.contentView.frame.height/4),
                    cell.subtitleLabel.topAnchor.constraint(equalTo: cell.numberLabel.bottomAnchor)
                    ])
                return cell
            default:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InfoCell", for: indexPath) as! InfoCell
                cell.numberLabel.text = "0"
                cell.subtitleLabel.text = "IDK"
                return cell
            }
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TipCell", for: indexPath) as! TipCell
            cell.textLabel.text = "Tip of the day: Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum"
            return cell
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath) as! MyCell
            cell.textLabel.text = String(indexPath.row + 1)
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath) as! MyCell
            cell.textLabel.text = String(indexPath.row + 1)
            return cell
        }
    }
}

extension HomeViewController: UICollectionViewDelegate {
    // Function called when an item is selected
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell = collectionView.cellForItem(at: indexPath) as! InfoCell
                print("Number of Photos: \(cell.numberLabel.text!)")
            case 1:
                let cell = collectionView.cellForItem(at: indexPath) as! InfoCell
                print("Number of Matches: \(cell.numberLabel.text!)")
            case 2:
                let cell = collectionView.cellForItem(at: indexPath) as! InfoCell
                print("Number of Faces: \(cell.numberLabel.text!)")
            default:
                print("This should be one of the \"number of\" metrics")
            }
        case 1:
            print("Tip of the day")
        case 2:
            print(indexPath.item + 1)
        default:
            print("This should be when clicking of the boxes, this statement should never be printed")
        }
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    // Setting size of cells for each section
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 0: // Box's showing number of photos, matches, and faces
            return CGSize(width: self.view.frame.width / 3 - 16, height: self.view.frame.width / 3 - 16)
        case 1: // Box showing tip of the day
            return CGSize(width: self.view.frame.width - 32, height: 100)
        case 2: // Box's showing uploaded photos
            return CGSize(width: self.view.frame.width / 3 - 16, height: self.view.frame.width / 3 - 16)
        default: // Must have this here for logic purposes, will more than likely never get to this case
            return CGSize(width: 100, height: 100)
        }
        //return CGSize(width: 100, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        switch section {
        case 0:
            return UIEdgeInsets.init(top: 8, left: 16, bottom: 8, right: 16)
        case 1:
            return UIEdgeInsets.init(top: 0, left: 16, bottom: 8, right: 16)
        case 2:
            return UIEdgeInsets.init(top: 8, left: 16, bottom: 8, right: 16)
        default:
            return UIEdgeInsets.init(top: 8, left: 16, bottom: 8, right: 16)
        }
    }
}

// Class for tip of the day cell
class TipCell: UICollectionViewCell {
    weak var textLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let textLabel = UILabel(frame: .zero)
        
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(textLabel)
        // Setting up autolayout constraints for text label inside of the cell
        NSLayoutConstraint.activate([
            textLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            textLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
            textLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 8),
            textLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -8)
            ])
        self.textLabel = textLabel
        
        self.contentView.backgroundColor = hexStringToUIColor(hex: "#F6F6F6") // background color of the cell
        // text label attributes
        self.textLabel.textColor = hexStringToUIColor(hex: "#33439B")
        self.textLabel.font = UIFont(name: "Lato-Regular", size: 17)
        self.textLabel.textAlignment = .left
        // the next two lines are used for automatic word wrapping
        self.textLabel.lineBreakMode = .byWordWrapping
        self.textLabel.numberOfLines = 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        fatalError("Interface Builder is not supported!")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        fatalError("Interface Builder is not supported!")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.textLabel!.text = nil
    }
}

class InfoCell: UICollectionViewCell {
    weak var numberLabel: UILabel!
    weak var subtitleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let numberLabel = UILabel(frame: .zero)
        let subtitleLabel = UILabel(frame: .zero)
        
        numberLabel.translatesAutoresizingMaskIntoConstraints = false
        subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(numberLabel)
        self.contentView.addSubview(subtitleLabel)
        NSLayoutConstraint.activate([
            numberLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 16),
            numberLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            
            subtitleLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 16),
            subtitleLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor)
            ])
        self.numberLabel = numberLabel
        self.subtitleLabel = subtitleLabel
        
        self.contentView.backgroundColor = hexStringToUIColor(hex: "#F6F6F6")
        // text label attributes
        self.numberLabel.textColor = hexStringToUIColor(hex: "#33439B")
        self.numberLabel.font = UIFont(name: "Lato-Bold", size: 25)
        self.numberLabel.textAlignment = .left
        self.subtitleLabel.textColor = hexStringToUIColor(hex: "#33439B")
        self.subtitleLabel.font = UIFont(name: "Lato-Bold", size: 16)
        self.subtitleLabel.textAlignment = .left
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        fatalError("Interface Builder is not supported!")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        fatalError("Interface Builder is not supported!")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.numberLabel.text = nil
        self.subtitleLabel.text = nil
    }
}

class MyCell: UICollectionViewCell {
    
    weak var textLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let textLabel = UILabel(frame: .zero)
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(textLabel)
        NSLayoutConstraint.activate([
            textLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            textLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
            textLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            textLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor)
            ])
        self.textLabel = textLabel
        
        self.contentView.backgroundColor = hexStringToUIColor(hex: "#B4B4B4")
        self.textLabel.textAlignment = .center
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        fatalError("Interface Builder is not supported!")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        fatalError("Interface Builder is not supported!")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.textLabel.text = nil
    }
}

extension UINavigationBarDelegate {
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}

extension UICollectionViewCell {
    // Converting hex string to UIColor object
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
