//
//  LaunchAnimationProgrammaticViewController.swift
//  Related Faces
//
//  Created by Daniel Stewart on 2/6/19.
//  Copyright © 2019 Related Faces. All rights reserved.
//

import UIKit
import AWSMobileClient

class LaunchAnimationProgrammaticViewController: UIViewController {
    let cognitoHelper = CognitoHelper()
    
    private let logo: UIImageView = {
        let logo = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 60))
        logo.image = UIImage(named: "logo")
        logo.contentMode = .scaleAspectFit
        logo.translatesAutoresizingMaskIntoConstraints = false
        return logo
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIViewController.hexStringToUIColor(hex: "#F6F6F6")
        
        view.addSubview(logo)
        logo.frame = CGRect(origin: CGPoint(x: view.center.x - 82, y: view.center.y - 29.5), size: CGSize(width: 164, height: 59))
        
        /*NSLayoutConstraint.activate([
            logo.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logo.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            logo.heightAnchor.constraint(equalToConstant: 60)
            ])*/
        
        //.sharedInstance().signOut()
        //cognitoHelper.signOut()
        
        UIView.animate(withDuration: 2.0, delay: 1.0, options: .curveEaseInOut, animations: {
            self.logo.transform = CGAffineTransform(translationX: 0, y:  -self.view.center.y + 70)
            self.view.layoutIfNeeded()
        }) { (finished: Bool) in
            if self.cognitoHelper.isSignedIn {
                print("Already signed in")
                let spinner = UIViewController.displaySpinner(onView: self.view)
                User.currentUser.clearCurrentUser()
                while(User.currentUser.isNotDoneGettingUser()) {} // Waiting until all user attributes are received from aws
                UIViewController.removeSpinner(spinner: spinner)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as UIViewController
                UINavigationBar.appearance().isHidden = false
                UINavigationBar.appearance().barTintColor = UIViewController.hexStringToUIColor(hex: "#33439B")
                UINavigationBar.appearance().tintColor = .white
                UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
                UINavigationBar.appearance().isTranslucent = false
                self.present(vc, animated: true, completion: nil)
            } else {
                print("Not already signed in")
                let vc = LoginProgrammaticViewController()
                self.present(vc, animated: false, completion: nil)
            }
        }
    }
}
