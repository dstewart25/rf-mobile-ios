//
//  ConfirmSignUpProgrammaticViewController.swift
//  Related Faces
//
//  Created by Daniel Stewart on 2/7/19.
//  Copyright © 2019 Related Faces. All rights reserved.
//

import UIKit
import AWSMobileClient

class ConfirmSignUpProgrammaticViewController: UIViewController {
    let cognitoHelper = CognitoHelper()
    var sentTo: String?
    var username: String?
    var password: String?
    
    private let logo: UIImageView = {
        let logo = UIImageView()
        logo.image = UIImage(named: "logo")
        logo.contentMode = .scaleAspectFit
        logo.translatesAutoresizingMaskIntoConstraints = false
        return logo
    }()
    
    private let confirmSignUpContentView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let sentToLabel: UILabel = {
        let label = UILabel()
        label.text = "Sent to: "
        label.textColor = hexStringToUIColor(hex: "#33439B")
        label.numberOfLines = 1
        label.textAlignment = .left
        label.font = UIFont(name: "Lato-Bold", size: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let usernameTextField: UITextField = {
        let textField = UITextField()
        //textField.backgroundColor = .white
        textField.borderStyle = .line
        textField.placeholder = "Username"
        textField.font = UIFont(name: "Lato-Regular", size: 17)
        textField.isEnabled = false
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let confirmationCodeTextField: UITextField = {
        let textField = UITextField()
        //textField.backgroundColor = .white
        textField.borderStyle = .line
        textField.placeholder = "Confirmation Code"
        textField.font = UIFont(name: "Lato-Regular", size: 17)
        textField.keyboardType = UIKeyboardType.numberPad
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let buttonView: UIStackView = {
        let resendButton: UIButton = {
            let button = UIButton(type: .system)
            button.setTitle("Resend Confirmation Code", for: .normal)
            button.titleLabel?.font = UIFont(name: "Lato-Bold", size: 17)
            button.setTitleColor(hexStringToUIColor(hex: "#33439B"), for: .normal)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self, action: #selector(onTapResend), for: .touchUpInside)
            return button
        }()
        
        let confirmButton: UIButton = {
            let button = UIButton(type: .system)
            button.backgroundColor = hexStringToUIColor(hex: "#33439B")
            button.setTitle("Confirm", for: .normal)
            button.titleLabel?.font = UIFont(name: "Lato-Bold", size: 17)
            button.tintColor = .white
            button.layer.cornerRadius = 5
            button.tag = 1
            //button.isEnabled = false
            //button.alpha = 0.5
            button.clipsToBounds = true
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self, action: #selector(onTapConfirm), for: .touchUpInside)
            button.addTarget(self, action: #selector(confirmButtonStartAnimation), for: .touchDown)
            button.addTarget(self, action: #selector(confirmButtonStopAnimation), for: .touchDragExit)
            return button
        }()
        
        let stackView = UIStackView(arrangedSubviews: [resendButton, confirmButton])
        stackView.axis = .horizontal
        stackView.distribution = UIStackView.Distribution.fillProportionally
        stackView.alignment = .center
        stackView.spacing = 3
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIViewController.hexStringToUIColor(hex: "#F6F6F6")
        
        confirmSignUpContentView.addSubview(sentToLabel)
        confirmSignUpContentView.addSubview(usernameTextField)
        confirmSignUpContentView.addSubview(confirmationCodeTextField)
        view.addSubview(logo)
        view.addSubview(confirmSignUpContentView)
        view.addSubview(buttonView)
        
        NSLayoutConstraint.activate([
            logo.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logo.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            logo.heightAnchor.constraint(equalToConstant: 59),
            
            confirmSignUpContentView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            confirmSignUpContentView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            confirmSignUpContentView.topAnchor.constraint(equalTo: logo.bottomAnchor, constant: 20),
            confirmSignUpContentView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -80),
            
            sentToLabel.topAnchor.constraint(equalTo: confirmSignUpContentView.topAnchor, constant: 40),
            sentToLabel.leftAnchor.constraint(equalTo: confirmSignUpContentView.leftAnchor, constant: 30),
            
            usernameTextField.topAnchor.constraint(equalTo: sentToLabel.bottomAnchor, constant: 10),
            usernameTextField.leftAnchor.constraint(equalTo: confirmSignUpContentView.leftAnchor, constant: 30),
            usernameTextField.rightAnchor.constraint(equalTo: confirmSignUpContentView.rightAnchor, constant: -30),
            usernameTextField.heightAnchor.constraint(equalToConstant: 30),
            
            confirmationCodeTextField.topAnchor.constraint(equalTo: usernameTextField.bottomAnchor, constant: 8),
            confirmationCodeTextField.leftAnchor.constraint(equalTo: confirmSignUpContentView.leftAnchor, constant: 30),
            confirmationCodeTextField.rightAnchor.constraint(equalTo: confirmSignUpContentView.rightAnchor, constant: -30),
            confirmationCodeTextField.heightAnchor.constraint(equalToConstant: 30),
            
            buttonView.topAnchor.constraint(equalTo: confirmSignUpContentView.bottomAnchor, constant: 20),
            buttonView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            buttonView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            buttonView.heightAnchor.constraint(equalToConstant: 40)
            ])
        
        self.usernameTextField.text = self.username ?? "No username"
        self.sentToLabel.text = "Code sent to: \(self.sentTo ?? "error")"
        
        // Watching fo rtouch out of keyboard to hide keyboard
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }
    
    @objc func onTapResend() {
        AWSMobileClient.sharedInstance().resendSignUpCode(username: username!, completionHandler: { (result, error) in
            if let signUpResult = result {
                print("A verification code has been sent via \(signUpResult.codeDeliveryDetails!.deliveryMedium) at \(signUpResult.codeDeliveryDetails!.destination!)")
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Code Resent",
                                                            message: "Code resent to \(signUpResult.codeDeliveryDetails?.destination! ?? " no message")",
                        preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            } else if let error = error {
                print("\(error.localizedDescription)")
            }
        })
    }
    
    @objc func onTapConfirm(_ sender: Any) {
        // make sure the sender is a button
        guard let button = sender as? UIButton else { return }
        
        // Animations on button
        UIView.animate(withDuration: 0.3) {
            button.transform = CGAffineTransform(scaleX: 1, y: 1)
            button.alpha = 1
        }
        
        guard let username = username, let password = password else {
            print("Problem getting username or password from previous screen")
            return
        }
        
        // Checking to make sure that the user has actually entered their confirmation code
        guard let confirmationCodeValue = self.confirmationCodeTextField.text, !confirmationCodeValue.isEmpty, confirmationCodeValue.count == 6 else {
            let alertController = UIAlertController(title: "Confirmation code missing.",
                                                    message: "Please enter a valid confirmation code.",
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:  nil)
            return
        }
        
        cognitoHelper.confirmSignUp(username: username, password: password, confirmationCode: confirmationCodeValue, sender: self)
    }
    
    @objc func confirmButtonStartAnimation(_ sender: Any) {
        // make sure the sender is a button
        guard let button = sender as? UIButton else { return }
        
        UIView.animate(withDuration: 0.3) {
            button.transform = CGAffineTransform(scaleX: 0.97, y: 0.97)
            button.alpha = 0.85
        }
    }
    
    @objc func confirmButtonStopAnimation(_ sender: Any) {
        // make sure the sender is a button
        guard let button = sender as? UIButton else { return }
        
        UIView.animate(withDuration: 0.3) {
            button.transform = CGAffineTransform(scaleX: 1, y: 1)
            button.alpha = 1
        }
    }
}
