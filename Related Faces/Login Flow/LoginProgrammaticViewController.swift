//
//  LoginProgrammaticViewController.swift
//  Related Faces
//
//  Created by Daniel Stewart on 2/1/19.
//  Copyright © 2019 Related Faces. All rights reserved.
//

import UIKit

class LoginProgrammaticViewController: UIViewController, UITextFieldDelegate {
    let cognitoHelper = CognitoHelper()
    
    private let logo: UIImageView = {
        let logo = UIImageView()
        logo.image = UIImage(named: "logo")
        logo.contentMode = .scaleAspectFit
        logo.translatesAutoresizingMaskIntoConstraints = false
        return logo
    }()
    
    private let loginContentView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let usernameTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = .white
        textField.borderStyle = .line
        textField.placeholder = "Username"
        textField.font = UIFont(name: "Lato-Regular", size: 17)
        textField.textContentType = UITextContentType.username
        textField.autocapitalizationType = .none
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let passwordTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = .white
        textField.borderStyle = .line
        textField.isSecureTextEntry = true
        textField.placeholder = "Password"
        textField.font = UIFont(name: "Lato-Regular", size: 17)
        textField.textContentType = .password
        textField.returnKeyType = .go
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = hexStringToUIColor(hex: "#33439B")
        button.setTitle("Login", for: .normal)
        button.titleLabel?.font = UIFont(name: "Lato-Bold", size: 17)
        button.tintColor = .white
        button.layer.cornerRadius = 5
        button.isEnabled = false
        button.alpha = 0.5
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(onTapLogin), for: .touchUpInside)
        button.addTarget(self, action: #selector(loginButtonStartAnimation), for: .touchDown)
        button.addTarget(self, action: #selector(loginButtonStopAnimation), for: .touchDragExit)
        return button
    }()
    
    private let loginLabel: UILabel = {
        let label = UILabel()
        label.text = "Login"
        label.textColor = hexStringToUIColor(hex: "#33439B")
        label.numberOfLines = 1
        label.textAlignment = .center
        label.font = UIFont(name: "Lato-Bold", size: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let signUpView: UIStackView = {
        let signUpLabel: UILabel = {
            let label = UILabel()
            label.text = "Not a member yet?"
            label.textColor = hexStringToUIColor(hex: "#33439B")
            label.numberOfLines = 1
            label.textAlignment = .center
            label.font = UIFont(name: "Lato-Regular", size: 15)
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        
        let signUpButton: UIButton = {
            let button = UIButton(type: .system)
            button.setTitle("Sign Up", for: .normal)
            button.titleLabel?.font = UIFont(name: "Lato-Bold", size: 15)
            button.setTitleColor(hexStringToUIColor(hex: "#33439B"), for: .normal)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self, action: #selector(onTapSignUp), for: .touchUpInside)
            return button
        }()
        
        let stackView = UIStackView(arrangedSubviews: [signUpLabel, signUpButton])
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        stackView.spacing = 3
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let tosAndPrivacyLabel: UILabel = {
        let label = UILabel()
        
        let attributedText = NSMutableAttributedString(string: "By logging in you agree to our ", attributes: [NSAttributedString.Key.font: UIFont(name: "Lato-Regular", size: 10)!])
        attributedText.append(NSAttributedString(string: "Terms of Service", attributes: [NSAttributedString.Key.font: UIFont(name: "Lato-Regular", size: 10)!, NSAttributedString.Key.link: "tos"]))
        attributedText.append(NSAttributedString(string: " and ", attributes: [NSAttributedString.Key.font: UIFont(name: "Lato-Regular", size: 10)!]))
        attributedText.append(NSAttributedString(string: "Privacy Policy", attributes: [NSAttributedString.Key.font: UIFont(name: "Lato-Regular", size: 10)!, NSAttributedString.Key.link: "pp"]))
        
        //label.text = "By logging in you agree to our Terms of Service and Privacy Policy"
        label.attributedText = attributedText
        label.textColor = hexStringToUIColor(hex: "#33439B")
        label.textAlignment = .center
        label.numberOfLines = 2
        label.font = UIFont(name: "Lato-Regular", size: 10)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = UIViewController.hexStringToUIColor(hex: "#F6F6F6")
        
        usernameTextField.delegate = self
        passwordTextField.delegate = self
        
        loginContentView.addSubview(loginLabel)
        loginContentView.addSubview(usernameTextField)
        loginContentView.addSubview(passwordTextField)
        loginContentView.addSubview(signUpView)
        loginContentView.addSubview(tosAndPrivacyLabel)
        view.addSubview(loginContentView)
        view.addSubview(loginButton)
        view.addSubview(logo)
        
        NSLayoutConstraint.activate([
            logo.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logo.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            logo.heightAnchor.constraint(equalToConstant: 59),
            
            loginContentView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            loginContentView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            loginContentView.topAnchor.constraint(equalTo: logo.bottomAnchor, constant: 20),
            loginContentView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -80),
            
            loginLabel.topAnchor.constraint(equalTo: loginContentView.topAnchor, constant: 40),
            loginLabel.centerXAnchor.constraint(equalTo: loginContentView.centerXAnchor),
            
            usernameTextField.topAnchor.constraint(equalTo: loginLabel.bottomAnchor, constant: 40),
            usernameTextField.leftAnchor.constraint(equalTo: loginContentView.leftAnchor, constant: 30),
            usernameTextField.rightAnchor.constraint(equalTo: loginContentView.rightAnchor, constant: -30),
            usernameTextField.heightAnchor.constraint(equalToConstant: 40),
            
            passwordTextField.topAnchor.constraint(equalTo: usernameTextField.bottomAnchor, constant: 10),
            passwordTextField.leftAnchor.constraint(equalTo: loginContentView.leftAnchor, constant: 30),
            passwordTextField.rightAnchor.constraint(equalTo: loginContentView.rightAnchor, constant: -30),
            passwordTextField.heightAnchor.constraint(equalToConstant: 40),
            
            signUpView.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 10),
            signUpView.centerXAnchor.constraint(equalTo: loginContentView.centerXAnchor),
            
            tosAndPrivacyLabel.bottomAnchor.constraint(equalTo: loginContentView.bottomAnchor, constant: -10),
            tosAndPrivacyLabel.centerXAnchor.constraint(equalTo: loginContentView.centerXAnchor),
            
            loginButton.topAnchor.constraint(equalTo: loginContentView.bottomAnchor, constant: 20),
            loginButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            loginButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            loginButton.heightAnchor.constraint(equalToConstant: 40)
            ])
        
        // Watching for touch out of keyboard to remove keyboard
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        // Watching for typing in keyboard to check if login button should become active
        NotificationCenter.default.addObserver(self, selector: #selector(textDidChange(_:)), name: UITextField.textDidChangeNotification, object: nil)
    }
    
    @objc func onTapLogin() {
        UIView.animate(withDuration: 0.3) {
            self.loginButton.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.loginButton.alpha = 1
        }
        
        // Checking for text in both text fields before connecting to AWS
        if (self.usernameTextField.text?.isEmpty ?? true || self.passwordTextField.text?.isEmpty ?? true) {
            return
        }
        
        // Getting text from both text fields
        guard let usernameValue = usernameTextField.text, !usernameValue.isEmpty, let passwordValue = passwordTextField.text, !passwordValue.isEmpty else {
            let alertController = UIAlertController(title: "Missing Username or Password",
                                                    message: "Please enter your username and password.",
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion:  nil)
            return
        }
        
        cognitoHelper.signIn(username: usernameValue, password: passwordValue, sender: self)
    }
    
    @objc func loginButtonStartAnimation() {
        UIView.animate(withDuration: 0.3) {
            self.loginButton.transform = CGAffineTransform(scaleX: 0.97, y: 0.97)
            self.loginButton.alpha = 0.85
        }
    }
    
    @objc func loginButtonStopAnimation() {
        UIView.animate(withDuration: 0.3) {
            self.loginButton.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.loginButton.alpha = 1
        }
    }
    
    @objc func onTapSignUp() {
        let signUpVC = SignUpProgrammaticViewController()
        self.present(signUpVC, animated: true, completion: nil)
    }
    
    // Runs when text is changed in text field
    @objc private func textDidChange(_ notification: Notification) {
        // Checking if both email and password are entered to enable continue button
        if usernameTextField.hasText && passwordTextField.hasText {
            loginButton.isEnabled = true
            loginButton.alpha = 1.0
        } else {
            loginButton.isEnabled = false
            loginButton.alpha = 0.5
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case usernameTextField: // if on email field, go to password field
            passwordTextField.becomeFirstResponder()
        default: // if on password field, remove keyboard
            passwordTextField.resignFirstResponder()
            onTapLogin()
        }
        
        return true
    }
}
