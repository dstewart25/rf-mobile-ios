//
//  SignUpProgrammaticViewController.swift
//  Related Faces
//
//  Created by Daniel Stewart on 2/7/19.
//  Copyright © 2019 Related Faces. All rights reserved.
//

import UIKit
import AWSMobileClient

class SignUpProgrammaticViewController: UIViewController, UITextFieldDelegate {
    let cognitoHelper = CognitoHelper()
    
    private let logo: UIImageView = {
        let logo = UIImageView()
        logo.image = UIImage(named: "logo")
        logo.contentMode = .scaleAspectFit
        logo.translatesAutoresizingMaskIntoConstraints = false
        return logo
    }()
    
    private let signUpContentView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let emailTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = .white
        textField.borderStyle = .line
        textField.placeholder = "Email"
        textField.font = UIFont(name: "Lato-Regular", size: 17)
        textField.textContentType = UITextContentType.emailAddress
        textField.autocapitalizationType = .none
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let passwordTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = .white
        textField.borderStyle = .line
        textField.isSecureTextEntry = true
        textField.placeholder = "Password"
        textField.font = UIFont(name: "Lato-Regular", size: 17)
        textField.textContentType = .newPassword
        textField.autocapitalizationType = .words
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let confirmPasswordTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = .white
        textField.borderStyle = .line
        textField.isSecureTextEntry = true
        textField.placeholder = "Confirm Password"
        textField.font = UIFont(name: "Lato-Regular", size: 17)
        textField.autocapitalizationType = .words
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let nameTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = .white
        textField.borderStyle = .line
        textField.placeholder = "Name"
        textField.font = UIFont(name: "Lato-Regular", size: 17)
        textField.textContentType = .name
        textField.autocapitalizationType = .words
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let usernameTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = .white
        textField.borderStyle = .line
        textField.placeholder = "Username"
        textField.font = UIFont(name: "Lato-Regular", size: 17)
        textField.textContentType = .username
        textField.autocapitalizationType = .none
        textField.returnKeyType = .go
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let buttonView: UIStackView = {
        let cancelButton: UIButton = {
            let button = UIButton(type: .system)
            button.setTitle("Cancel", for: .normal)
            button.titleLabel?.font = UIFont(name: "Lato-Bold", size: 17)
            button.setTitleColor(hexStringToUIColor(hex: "#33439B"), for: .normal)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self, action: #selector(onTapCancel), for: .touchUpInside)
            return button
        }()
        
        let signUpButton: UIButton = {
            let button = UIButton(type: .system)
            button.backgroundColor = hexStringToUIColor(hex: "#33439B")
            button.setTitle("Sign Up", for: .normal)
            button.titleLabel?.font = UIFont(name: "Lato-Bold", size: 17)
            button.tintColor = .white
            button.layer.cornerRadius = 5
            button.tag = 1
            button.isEnabled = false
            button.alpha = 0.5
            button.clipsToBounds = true
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self, action: #selector(onTapSignUp), for: .touchUpInside)
            button.addTarget(self, action: #selector(signUpButtonStartAnimation), for: .touchDown)
            button.addTarget(self, action: #selector(signUpButtonStopAnimation), for: .touchDragExit)
            return button
        }()
        
        let stackView = UIStackView(arrangedSubviews: [cancelButton, signUpButton])
        stackView.axis = .horizontal
        stackView.distribution = UIStackView.Distribution.fillProportionally
        stackView.alignment = .center
        stackView.spacing = 3
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let signUpLabel: UILabel = {
        let label = UILabel()
        label.text = "Sign Up"
        label.textColor = hexStringToUIColor(hex: "#33439B")
        label.numberOfLines = 1
        label.textAlignment = .center
        label.font = UIFont(name: "Lato-Bold", size: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIViewController.hexStringToUIColor(hex: "#F6F6F6")
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        nameTextField.delegate = self
        usernameTextField.delegate = self
        
        let inputTextFields: UIStackView = {
            let stackView = UIStackView(arrangedSubviews: [emailTextField, passwordTextField, confirmPasswordTextField, nameTextField, usernameTextField])
            stackView.axis = .vertical
            stackView.distribution = .fillProportionally
            stackView.spacing = 10
            stackView.alignment = .fill
            stackView.translatesAutoresizingMaskIntoConstraints = false
            return stackView
        }()
        
        signUpContentView.addSubview(signUpLabel)
        signUpContentView.addSubview(inputTextFields)
        view.addSubview(signUpContentView)
        view.addSubview(buttonView)
        view.addSubview(logo)
        
        NSLayoutConstraint.activate([
            logo.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logo.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            logo.heightAnchor.constraint(equalToConstant: 59),
            
            signUpContentView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            signUpContentView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            signUpContentView.topAnchor.constraint(equalTo: logo.bottomAnchor, constant: 20),
            signUpContentView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -80),
            
            signUpLabel.topAnchor.constraint(equalTo: signUpContentView.topAnchor, constant: 20),
            signUpLabel.centerXAnchor.constraint(equalTo: signUpContentView.centerXAnchor),
            
            inputTextFields.topAnchor.constraint(equalTo: signUpLabel.bottomAnchor, constant: 40),
            inputTextFields.centerXAnchor.constraint(equalTo: signUpContentView.centerXAnchor),
            inputTextFields.leftAnchor.constraint(equalTo: signUpContentView.leftAnchor, constant: 20),
            inputTextFields.rightAnchor.constraint(equalTo: signUpContentView.rightAnchor, constant: -20),
            
            buttonView.topAnchor.constraint(equalTo: signUpContentView.bottomAnchor, constant: 20),
            buttonView.leftAnchor.constraint(equalTo: view.centerXAnchor),
            buttonView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
            buttonView.heightAnchor.constraint(equalToConstant: 40)
            ])
        
        // Watching for touch out of keyboard to remove keyboard
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        NotificationCenter.default.addObserver(self, selector: #selector(textDidChange(_:)), name: UITextField.textDidChangeNotification, object: nil)
    }
    
    @objc func onTapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func onTapSignUp(_ sender: Any) {
        // make sure the sender is a button
        guard let button = sender as? UIButton else { return }
        
        // Doing some animation work on the button
        UIView.animate(withDuration: 0.3) {
            button.transform = CGAffineTransform(scaleX: 1, y: 1)
            button.alpha = 1
        }
        
        // Checking to make sure that all of the text fields have values
        guard let emailValue = self.emailTextField.text, !emailValue.isEmpty,
            let passwordValue = self.passwordTextField.text, !passwordValue.isEmpty, let nameValue = self.nameTextField.text, !nameValue.isEmpty, let usernameValue = self.usernameTextField.text, !usernameValue.isEmpty else {
                let alertController = UIAlertController(title: "Missing Required Fields",
                                                        message: "All fields are required for registration.",
                                                        preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion:  nil)
                return
        }
        
        // Checking to make suyer that the password fields match
        guard self.passwordTextField.text! == self.confirmPasswordTextField.text! else {
            let alertController = UIAlertController(title: "Passwords Do Not Match", message: "Please re-enter your password.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default) { (action: UIAlertAction) in
                self.passwordTextField.text = nil
                self.confirmPasswordTextField.text = nil
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        // Attributes needed to sign up with AWS Cognito
        let attributes = [
            "email": emailValue,
            "name": nameValue
        ]
        
        cognitoHelper.signUp(username: usernameValue, password: passwordValue, attributes: attributes, sender: self)
    }
    
    @objc func signUpButtonStartAnimation(_ sender: Any) {
        // make sure the sender is a button
        guard let button = sender as? UIButton else { return }
        
        UIView.animate(withDuration: 0.3) {
            button.transform = CGAffineTransform(scaleX: 0.97, y: 0.97)
            button.alpha = 0.85
        }
    }
    
    @objc func signUpButtonStopAnimation(_ sender: Any) {
        // make sure the sender is a button
        guard let button = sender as? UIButton else { return }
        
        UIView.animate(withDuration: 0.3) {
            button.transform = CGAffineTransform(scaleX: 1, y: 1)
            button.alpha = 1
        }
    }
    
    // Runs when text is changed in text field
    @objc private func textDidChange(_ notification: Notification) {
        var button: UIButton?
        for subview in self.buttonView.arrangedSubviews {
            if subview.isKind(of: UIButton.self) && subview.tag == 1 {
                button = subview as? UIButton
            }
        }
        
        // Checking if both email and password are entered to enable continue button
        if emailTextField.hasText && passwordTextField.hasText && confirmPasswordTextField.hasText && nameTextField.hasText && usernameTextField.hasText {
            button?.isEnabled = true
            button?.alpha = 1.0
        } else {
            button?.isEnabled = false
            button?.alpha = 0.5
        }
    }
    
    // "Return" button clicked on keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextField: // if on email field, go to password field
            passwordTextField.becomeFirstResponder()
        case passwordTextField: // if on password, go to confirm password
            confirmPasswordTextField.becomeFirstResponder()
        case confirmPasswordTextField: // if on confirm password, go to first name
            nameTextField.becomeFirstResponder()
        case nameTextField: // if on last name, go to username
            usernameTextField.becomeFirstResponder()
        default: // if on username field, remove keyboard
            usernameTextField.resignFirstResponder()
            onTapSignUp(self)
        }
        
        return true
    }
}
