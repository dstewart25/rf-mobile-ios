//
//  MenuViewController.swift
//  Related Faces
//
//  Created by Daniel Stewart on 9/23/18.
//  Copyright © 2018 Related Faces. All rights reserved.
//

import UIKit
import AWSMobileClient

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var menuNameArray:Array = [String]() // Holds name of cells
    var menuIcons:Array = [UIImage]() // Holds image for cell
    var profileImage = UIImage() // Holds profile image
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Making name label empty
        self.nameLabel.text = ""

        // Name of table cells and icons for each table cell
        menuNameArray = ["Home","Messages","Notifications","Take/Upload Photo","Account","View/Search Photos","Logout"]
        menuIcons = [UIImage(named: "ic_menu_home")!,UIImage(named: "ic_menu_send")!,UIImage(named: "ic_menu_notifications")!,UIImage(named: "ic_menu_upload")!,UIImage(named: "ic_menu_account")!,UIImage(named: "ic_menu_search")!,UIImage(named: "ic_menu_logout")!]
        
        // Profile image initialization
        profileImage = UIImage(named: "account")!
        
        //userImage.layer.backgroundColor = hexStringToCGColor(hex: "#33439B")
        let userImageColor = UIViewController.hexStringToUIColor(hex: "#33439B")
        userImage.image = profileImage
        userImage.tintColor = .white
        userImage.layer.backgroundColor = userImageColor.cgColor
        userImage.layer.borderWidth = 1
        userImage.layer.cornerRadius = 0.5 * userImage.bounds.size.width
        userImage.layer.masksToBounds = false
        userImage.clipsToBounds = true
        
        self.nameLabel.text = User.currentUser.userAttributes?.name
        
        UINavigationBar.appearance().barTintColor = UIViewController.hexStringToUIColor(hex: "#33439B")
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNameArray.count
    }
    
    // Setting up table cells with image and text
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
        
        // All of these cases below are to disable the menu items that are not current implemented
        switch indexPath.row {
        case 1:
            cell.menuItemIcon.image = menuIcons[indexPath.row]
            cell.menuItemIcon.tintColor = .white
            cell.menuItemIcon.alpha = 0.1
            cell.menuItemLabel.text! = menuNameArray[indexPath.row]
            cell.menuItemLabel.alpha = 0.1
            return cell
        case 2:
            cell.menuItemIcon.image = menuIcons[indexPath.row]
            cell.menuItemIcon.tintColor = .white
            cell.menuItemIcon.alpha = 0.1
            cell.menuItemLabel.text! = menuNameArray[indexPath.row]
            cell.menuItemLabel.alpha = 0.1
            return cell
        case 4:
            cell.menuItemIcon.image = menuIcons[indexPath.row]
            cell.menuItemIcon.tintColor = .white
            cell.menuItemIcon.alpha = 0.1
            cell.menuItemLabel.text! = menuNameArray[indexPath.row]
            cell.menuItemLabel.alpha = 0.1
            return cell
        case 5:
            cell.menuItemIcon.image = menuIcons[indexPath.row]
            cell.menuItemIcon.tintColor = .white
            cell.menuItemIcon.alpha = 0.1
            cell.menuItemLabel.text! = menuNameArray[indexPath.row]
            cell.menuItemLabel.alpha = 0.1
            return cell
        default:
            cell.menuItemIcon.image = menuIcons[indexPath.row]
            cell.menuItemIcon.tintColor = .white
            cell.menuItemIcon.alpha = 0.5
            cell.menuItemLabel.text! = menuNameArray[indexPath.row]
            return cell
        }
        
        /*cell.menuItemIcon.image = menuIcons[indexPath.row]
        cell.menuItemIcon.tintColor = .white
        cell.menuItemIcon.alpha = 0.5
        cell.menuItemLabel.text! = menuNameArray[indexPath.row]
        return cell*/
    }
    
    // Action of clicking cell in table and moving to new view
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let revealViewController:SWRevealViewController = self.revealViewController()
        
        let cell:MenuTableViewCell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell // Finding cell that was tapped
        
        tableView.deselectRow(at: indexPath, animated: true) // Deselecting and animating row in UITableView when clicked
        
        // If statements to find which View Controller to switch to
        if cell.menuItemLabel.text! == "Home" {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            let newFrontViewController = UINavigationController.init(rootViewController: desController)
            
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
        }
        /*if cell.menuItemLabel.text! == "Messages" {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
            let newFrontViewController = UINavigationController.init(rootViewController: desController)
            
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
        }
        if cell.menuItemLabel.text! == "Notifications" {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
            let newFrontViewController = UINavigationController.init(rootViewController: desController)
            
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
        }*/
        if cell.menuItemLabel.text! == "Take/Upload Photo" {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Camera", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "ScanPhotoViewController") as! ScanPhotoViewController
            let newFrontViewController = UINavigationController.init(rootViewController: desController)
            
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
        }
        /*if cell.menuItemLabel.text! == "Account" {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "AccountViewController") as! AccountViewController
            let newFrontViewController = UINavigationController.init(rootViewController: desController)
            
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
        }
        if cell.menuItemLabel.text! == "View/Search Photos" {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "ViewPhotosViewController") as! ViewPhotosViewController
            let newFrontViewController = UINavigationController.init(rootViewController: desController)
            
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
        }*/
        if cell.menuItemLabel.text! == "Logout" {
            // Signing out
            AWSMobileClient.sharedInstance().signOut()
            let vc = LoginProgrammaticViewController()
            
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = CATransitionType.reveal
            transition.subtype = CATransitionSubtype.fromBottom
            transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.default)
            view.window!.layer.add(transition, forKey: kCATransition)
            present(vc, animated: false, completion: nil)
        }
    }
    
    // Sets height of individual cells in UITableView in pixels
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
