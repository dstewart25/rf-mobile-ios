//
//  CognitoHelperTool.swift
//  Related Faces
//
//  Created by Daniel Stewart on 2/13/19.
//  Copyright © 2019 Related Faces. All rights reserved.
//

import Foundation
import AWSMobileClient

class CognitoHelper: NSObject, CognitoHelperProtocol {
    var isSignedIn: Bool
    
    override init() {
        isSignedIn = false
        super.init()
        
        checkSignedIn()
    }
    
    func signIn(username: String, password: String, sender: UIViewController) {
        
        let spinner = UIViewController.displaySpinner(onView: sender.view)
        
        AWSMobileClient.sharedInstance().signIn(username: username, password: password) { (signInResult, error) in
            UIViewController.removeSpinner(spinner: spinner)
            if let error = error  {
                print("\(error)")
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Error", message: "An error occured: \(String(describing: error))", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alertController.addAction(okAction)
                    sender.present(alertController, animated: true, completion: nil)
                }
            } else if let signInResult = signInResult {
                DispatchQueue.main.async {
                    switch signInResult.signInState {
                    case .signedIn:
                        print("Logged In")
                        User.currentUser.clearCurrentUser()
                        while(User.currentUser.isNotDoneGettingUser()) {} // Waiting until all user attributes are received from aws
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as UIViewController
                        UINavigationBar.appearance().isHidden = false
                        UINavigationBar.appearance().barTintColor = UIViewController.hexStringToUIColor(hex: "#33439B")
                        UINavigationBar.appearance().tintColor = .white
                        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
                        UINavigationBar.appearance().isTranslucent = false
                        sender.present(vc, animated: true, completion: nil)
                    default:
                        print("Case Unknown, This will never be hit")
                    }
                }
            }
        }
    }
    
    func signUp(username: String, password: String, attributes: [String:String], sender: UIViewController) {
        let spinner = UIViewController.displaySpinner(onView: sender.view)
        
        AWSMobileClient.sharedInstance().signUp(username: username, password: password, userAttributes: attributes) { (signUpResult, error) in
            print("trying to sign up")
            UIViewController.removeSpinner(spinner: spinner) // ending spinner loading animation
            if let signUpResult = signUpResult {
                switch(signUpResult.signUpConfirmationState) {
                case .confirmed:
                    print("User is signed up and confirmed.")
                case .unconfirmed:
                    print("User is not confirmed and needs verification via \(signUpResult.codeDeliveryDetails!.deliveryMedium) sent at \(signUpResult.codeDeliveryDetails!.destination!)")
                    DispatchQueue.main.async {
                        let confirmVC = ConfirmSignUpProgrammaticViewController()
                        confirmVC.sentTo = signUpResult.codeDeliveryDetails?.destination
                        confirmVC.username = username
                        confirmVC.password = password
                        sender.present(confirmVC, animated: true, completion: nil)
                    }
                case .unknown:
                    print("Unexpected case")
                }
            } else if let error = error {
                if let error = error as? AWSMobileClientError {
                    switch(error) {
                    case .usernameExists(let message):
                        print(message)
                    default:
                        break
                    }
                }
                print("\(error.localizedDescription)")
                print(error)
            }
        }
    }
    
    func confirmSignUp(username: String, password: String, confirmationCode: String, sender: UIViewController) {
        let spinner = UIViewController.displaySpinner(onView: sender.view) // showing spinner loading animation
        
        AWSMobileClient.sharedInstance().confirmSignUp(username: username, confirmationCode: confirmationCode) { (signUpResult, error) in
            if let signUpResult = signUpResult {
                switch(signUpResult.signUpConfirmationState) {
                case .confirmed:
                    print("User is signed up and confirmed.")
                case .unconfirmed:
                    print("User is not confirmed and needs verification via \(signUpResult.codeDeliveryDetails!.deliveryMedium) sent at \(signUpResult.codeDeliveryDetails!.destination!)")
                case .unknown:
                    print("Unexpected case")
                }
            } else if let error = error {
                print("\(error.localizedDescription)")
                print(error)
            }
            
            DispatchQueue.main.async {
                UIViewController.removeSpinner(spinner: spinner)
                let _ = self.signIn(username: username, password: password, sender: sender)
            }
        }
    }
    
    func signOut() -> Bool {
        AWSMobileClient.sharedInstance().signOut()
        return true
    }
    
    func getUserAttributes() -> Bool {
        let group = DispatchGroup()
        group.enter()
        
        var successful = false
        AWSMobileClient.sharedInstance().getUserAttributes { (attributes, error) in
            if let error = error {
                print(error)
            }
            
            if let _ = attributes {
                successful = true
            }
            
            group.leave()
        }
        
        group.wait()
        return successful
    }
    
    func getUserTokens() -> Bool {
        let group = DispatchGroup()
        group.enter()
        
        var successful = false
        AWSMobileClient.sharedInstance().getTokens { (tokens, error) in
            if let error = error {
                print(error)
            }
            
            if let _ = tokens {
                successful = true
            }
            
            group.leave()
        }
        
        group.wait()
        return successful
    }
    
    func getUserIdToken() -> String {
        let group = DispatchGroup()
        group.enter()
        var idToken = ""
        
        AWSMobileClient.sharedInstance().getTokens { (tokens, error) in
            if let error = error {
                print(error)
            }
            
            if let tokens = tokens {
                idToken = (tokens.idToken?.tokenString)!
            }
            group.leave()
        }
        
        group.wait()
        return idToken
    }
    
    private func checkSignedIn() {
        AWSMobileClient.sharedInstance().initialize { (userState, error) in
            if let userState = userState {
                switch(userState){
                case .signedIn:
                    self.isSignedIn = true
                case .signedOut:
                    self.isSignedIn = false
                default:
                    print("default? signing out")
                    let _ = self.signOut()
                    self.isSignedIn = false
                }
            } else if let error = error {
                print("error: \(error.localizedDescription)")
                print(error)
                let _ = self.signOut()
                self.isSignedIn = false
            }
        }
    }
}
