//
//  Login.swift
//  Related Faces
//
//  Created by Daniel Stewart on 2/13/19.
//  Copyright © 2019 Related Faces. All rights reserved.
//

import Foundation

protocol CognitoHelperProtocol {
    var isSignedIn: Bool { get }
    
    func signIn(username: String, password: String, sender: UIViewController)
    func signUp(username: String, password: String, attributes: [String:String], sender: UIViewController)
    func confirmSignUp(username: String, password: String, confirmationCode: String, sender: UIViewController)
    func signOut() -> Bool
    func getUserAttributes() -> Bool
    func getUserTokens() -> Bool
}
