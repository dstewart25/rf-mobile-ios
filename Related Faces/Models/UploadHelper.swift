//
//  UploadHelper.swift
//  Related Faces
//
//  Created by Daniel Stewart on 2/13/19.
//  Copyright © 2019 Related Faces. All rights reserved.
//

import Foundation

class UploadHelper: NSObject, UploadHelperProtocol {
    struct CaptureData {
        let pictureId: String
        let transId: String
        
        init(json: [String: Any]) {
            pictureId = json["pictureId"] as? String ?? ""
            transId = json["transId"] as? String ?? ""
        }
    }
    
    func uploadImage(with data: Data, idToken: String, sender: UIViewController) {
        
        let spinner = UIViewController.displaySpinner(onView: sender.view) // showing spinner loading animation
        // change the method name, or path or the query string parameters here as desired
        let httpMethodName = "POST"
        // change to any valid path you configured in the API
        let URLString = "/picture/capture"
        let queryStringParameters:[String:String] = [:]
        let boundary = "Boundary-\(UUID().uuidString)"
        let headerParameters = [
            "Authorization": idToken,
            "Content-Type": "multipart/form-data; boundary=" + boundary
        ]
        
        let body = createBody(boundary: boundary, data: data, mimeType: "image/jpeg", filename: "")
        
        // Construct the request object
        let apiRequest = AWSAPIGatewayRequest(httpMethod: httpMethodName,
                                              urlString: URLString,
                                              queryParameters: queryStringParameters,
                                              headerParameters: headerParameters,
                                              httpBody: body)
        
        // Create a service configuration
        let serviceConfiguration = AWSServiceConfiguration(region: AWSRegionType.USEast2,
                                                           credentialsProvider: nil)
        
        // Initialize the API client using the service configuration
        NEWRfservicesClient.registerClient(withConfiguration: serviceConfiguration!, forKey: "")
        
        // Fetch the Cloud Logic client to be used for invocation
        let invocationClient = NEWRfservicesClient.client(forKey: "")
        
        invocationClient.invoke(apiRequest).continueWith { (task: AWSTask) -> Any? in
            if let error = task.error {
                print("Error occurred: \(error)")
                print(error.localizedDescription)
                // Handle error here
                return nil
            }
            
            // Handle successful result here
            let result = task.result!
            let responseString = String(data: result.responseData!, encoding: .utf8)
            
            print(responseString!)
            //print(result.rawResponse)
            print(result.statusCode)
            
            if result.statusCode == 200 {
                do {
                    guard let json = try JSONSerialization.jsonObject(with: result.responseData!, options: .mutableContainers) as? [String: Any] else {
                        print("Error on parsing json")
                        return nil
                    }
                    
                    print("Successfully parsed json")
                    let captureData = CaptureData(json: json)
                    print("pictureId: \(captureData.pictureId)")
                    print("transId: \(captureData.transId)")
                    
                    let request = [
                        "pictureId": captureData.pictureId,
                        "transId": captureData.transId
                    ]
                    self.acceptPhoto(idToken: idToken, request: request, spinner: spinner, sender: sender)
                } catch let jsonError {
                    print("Error serializing json:", jsonError)
                }
            }
            
            return nil
        }
    }
    
    private func createBody(boundary: String, data: Data, mimeType: String, filename: String) -> Data {
        let body = NSMutableData()
        
        let boundaryPrefix = "--\(boundary)\r\n"
        
        body.appendString(boundaryPrefix)
        body.appendString("Content-Disposition: form-data; name=\"fileName\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimeType)\r\n\r\n")
        body.append(data)
        body.appendString("\r\n")
        body.appendString("--".appending(boundary.appending("--")))
        
        return body as Data
    }
    
    func acceptPhoto(idToken: String, request: [AnyHashable:AnyHashable], spinner: UIView, sender: UIViewController) {
        // change the method name, or path or the query string parameters here as desired
        let httpMethodName = "POST"
        // change to any valid path you configured in the API
        let URLString = "/picture/accept"
        let queryStringParameters: [String:String] = [:]
        let headerParameters: [String:String] = [
            "Authorization": idToken
        ]
        
        // Construct the request object
        let apiRequest = AWSAPIGatewayRequest(httpMethod: httpMethodName,
                                              urlString: URLString,
                                              queryParameters: queryStringParameters,
                                              headerParameters: headerParameters,
                                              httpBody: request)
        
        // Create a service configuration
        let serviceConfiguration = AWSServiceConfiguration(region: AWSRegionType.USEast2,
                                                           credentialsProvider: nil)
        
        // Initialize the API client using the service configuration
        NEWRfservicesClient.registerClient(withConfiguration: serviceConfiguration!, forKey: "")
        
        // Fetch the Cloud Logic client to be used for invocation
        let invocationClient = NEWRfservicesClient.client(forKey: "")
        
        invocationClient.invoke(apiRequest).continueWith { (task: AWSTask) -> Any? in
            UIViewController.removeSpinner(spinner: spinner) // ending spinner loading animation
            if let error = task.error {
                print("Error occurred: \(error)")
                print(error.localizedDescription)
                // Handle error here
                return nil
            }
            
            // Handle successful result here
            let result = task.result!
            let responseString = String(data: result.responseData!, encoding: .utf8)
            
            print(responseString!)
            print(result.rawResponse)
            print(result.statusCode)
            
            DispatchQueue.main.async {
                let alertController = UIAlertController(title: "Upload Successful", message: "Your image has been uploaded. Please go to our website to finish uploading the image.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
                alertController.addAction(okAction)
                sender.present(alertController, animated: true, completion: nil)
            }
            
            DispatchQueue.main.async {
                sender.dismiss(animated: true, completion: nil)
            }
            
            return nil
        }
    }
}

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}
