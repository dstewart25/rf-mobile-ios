//
//  UploadHelperProtocol.swift
//  Related Faces
//
//  Created by Daniel Stewart on 2/13/19.
//  Copyright © 2019 Related Faces. All rights reserved.
//

import Foundation

protocol UploadHelperProtocol {
    func uploadImage(with data: Data, idToken: String, sender: UIViewController)
    func acceptPhoto(idToken: String, request: [AnyHashable:AnyHashable], spinner: UIView, sender: UIViewController)
}
