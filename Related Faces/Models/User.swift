//
//  User.swift
//  Related Faces
//
//  Created by Daniel Stewart on 1/22/19.
//  Copyright © 2019 Related Faces. All rights reserved.
//

import Foundation
import AWSMobileClient

class User {
    // MARK: - Attributes
    static let currentUser = User()
    
    // MARK: -
    var userAttributes: UserAttributes?
    var token: Tokens?
    
    private var hasAttributes: Bool = false
    private var hasToken: Bool = false
    
    // Initialization
    private init() {
        self.getUserAttributes()
        self.getTokens()
    }
    
    func isNotDoneGettingUser() -> Bool {
        if hasAttributes && hasToken {
            return false
        } else {
            return true
        }
    }
    
    func clearCurrentUser() {
        self.hasAttributes = false
        self.hasToken = false
        self.getUserAttributes()
        self.getTokens()
    }
    
    private func getTokens() {
        AWSMobileClient.sharedInstance().getTokens { (tokens, error) in
            guard let tokens = tokens else {
                print("Unable to get tokens")
                return
            }
            
            if let error = error {
                print(error)
                return
            }
            
            self.token = tokens
            self.hasToken = true
        }
    }
    
    private func getUserAttributes() {
        AWSMobileClient.sharedInstance().getUserAttributes { (receivedAttributes, error) in
            guard let receivedAttributes = receivedAttributes else {
                print("Unable to get user attributes")
                return
            }
            
            if let error = error {
                print(error)
                return
            }
            
            var receivedUserAttributes = UserAttributes()
            
            for attribute in receivedAttributes {
                switch attribute.key{
                case "name":
                    receivedUserAttributes.name = attribute.value
                case "email":
                    receivedUserAttributes.email = attribute.value
                case "email_verified":
                    receivedUserAttributes.isEmailVerified = attribute.value.toBool()!
                case "sub":
                    receivedUserAttributes.sub = attribute.value
                default:
                    print("The attribute: \(attribute.key), with value: \(attribute.value) is not currently being stored in the UserAttributes struct.")
                }
            }
            
            self.userAttributes = receivedUserAttributes
            self.hasAttributes = true
        }
    }
}

struct UserAttributes {
    var username: String?
    var email: String?
    var name: String?
    var isEmailVerified: Bool?
    var sub: String? // I don't actually know what this information is but it it being sent from Cognito so saving anyways
}

extension String {
    func toBool() -> Bool? {
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return nil
        }
    }
}
