//
//  NotificationsViewController.swift
//  Related Faces
//
//  Created by Daniel Stewart on 9/25/18.
//  Copyright © 2018 Related Faces. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if self.revealViewController() != nil {
            menuButton.target = revealViewController() // this is the button located on the UINavigation bar
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
            self.view.addGestureRecognizer((self.revealViewController()?.tapGestureRecognizer())!)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
