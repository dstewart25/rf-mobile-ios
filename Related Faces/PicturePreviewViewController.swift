//
//  PicturePreviewViewController.swift
//  Related Faces
//
//  Created by Daniel Stewart on 10/17/18.
//  Copyright © 2018 Related Faces. All rights reserved.
//

import UIKit
import AVFoundation
import AWSMobileClient
import AWSAPIGateway

class PicturePreviewViewController: UIViewController {
    let uploadHelper = UploadHelper()
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var usePictureButton: UIButton!
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var retakePictureButton: UIButton!
    
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imagePreview.image = image
    }
    
    // Removes image from memory and brings user back to Camera view
    @IBAction func retakePictureTapped(_ sender: Any) {
        imagePreview.image = nil
        image = nil
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onTapUsePicture(_ sender: Any) {
        if let imageData = image?.jpegData(compressionQuality: 0.99) {
            uploadHelper.uploadImage(with: imageData, idToken: (User.currentUser.token?.idToken?.tokenString)!, sender: self)
        } else {
            print("Unable to convert UIImage to JPEG Data")
        }
    }
}
