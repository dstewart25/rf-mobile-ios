//
//  ScanPhotoViewController.swift
//  Related Faces
//
//  Created by Daniel Stewart on 9/26/18.
//  Copyright © 2018 Related Faces. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class ScanPhotoViewController: UIViewController, UINavigationControllerDelegate {
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var photoLibraryButton: UIButton!
    @IBOutlet weak var switchCameraButton: UIButton!
    @IBOutlet weak var navBar: UINavigationItem!
    
    var captureSession: AVCaptureSession?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var capturePhotoOutput: AVCapturePhotoOutput?
    var image: UIImage?
    static var onBackCamera = true
    
    override func viewDidAppear(_ animated: Bool) {
        // Settig up actions to use the menu reveal functionality
        if self.revealViewController() != nil {
            menuButton.target = revealViewController() // this is the button located on the UINavigation bar
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:)) // tap action on button to open menu
            self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!) // swipe gesture to close or open menu
            self.view.addGestureRecognizer((self.revealViewController()?.tapGestureRecognizer())!) // tap gesture to close menu
        }
        
        navBar.title = "Upload"
        
        var cameraSide: AVCaptureDevice?
        
        if ScanPhotoViewController.onBackCamera { // Setting AVCaptureDevice to back camera
            cameraSide = AVCaptureDevice.default(for: .video)
        } else { // Setting up AVCaptureDevice to the front camera
            cameraSide = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front)
        }
        
        guard let camera = cameraSide else {
                print("Unable to access camera!")
                return
        }
        
        do {
            // setting up middle man between input and output for capturing from the capture device
            let input = try AVCaptureDeviceInput(device: camera)
            
            // setting up session to be able to actually take pictures
            captureSession = AVCaptureSession()
            captureSession?.addInput(input)
            
            // Get an instance of ACCapturePhotoOutput class
            capturePhotoOutput = AVCapturePhotoOutput()
            capturePhotoOutput?.isHighResolutionCaptureEnabled = true
            // Set the output on the capture session
            captureSession?.addOutput(capturePhotoOutput!)
        } catch let error {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
        
        // configuring view to show live preview of the camera
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        previewView.layer.addSublayer(videoPreviewLayer!)
        
        print("Starting Camera Session")
        captureSession?.startRunning()
    }
    
    @IBAction func onTapTakePhoto(_ sender: Any) {
        // Make sure capturePhotoOutput is valid
        guard let capturePhotoOutput = self.capturePhotoOutput else { return }
        // Get an instance of AVCapturePhotoSettings class
        let photoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        // Set photo settings for our need
        photoSettings.isAutoStillImageStabilizationEnabled = true
        photoSettings.isHighResolutionPhotoEnabled = true
        photoSettings.flashMode = .off
        // Call capturePhoto method by passing our photo settings and a
        // delegate implementing AVCapturePhotoCaptureDelegate
        capturePhotoOutput.capturePhoto(with: photoSettings, delegate: self)
    }
    
    @IBAction func onTapLibrary(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func onTapSwitchCamera(_ sender: Any) {
        print("Switch camera side")
        self.captureSession?.stopRunning()
        
        var switchedCamera: AVCaptureDevice?
        
        if ScanPhotoViewController.onBackCamera { // Currently showing back camera, switch to front camera
            switchedCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front)
            ScanPhotoViewController.onBackCamera = false
        } else {
            switchedCamera = AVCaptureDevice.default(for: .video)
            ScanPhotoViewController.onBackCamera = true
        }
        
        guard let camera = switchedCamera else {
            print("Unable to access new side of camera!")
            self.captureSession?.startRunning()
            return
        }
        
        do {
            // setting up middle man between input and output for capturing from the capture device
            let input = try AVCaptureDeviceInput(device: camera)
            
            // setting up session to be able to actually take pictures
            captureSession = AVCaptureSession()
            captureSession?.addInput(input)
            
            // Get an instance of ACCapturePhotoOutput class
            capturePhotoOutput = AVCapturePhotoOutput()
            capturePhotoOutput?.isHighResolutionCaptureEnabled = true
            // Set the output on the capture session
            captureSession?.addOutput(capturePhotoOutput!)
        } catch let error {
            print("Error Unable to initialize front camera:  \(error.localizedDescription)")
        }
        
        // configuring view to show live preview of the camera
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        previewView.layer.addSublayer(videoPreviewLayer!)
        
        print("Starting Camera Session")
        captureSession?.startRunning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("Stopping Camera Session")
        super.viewWillDisappear(animated)
        self.captureSession?.stopRunning()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension ScanPhotoViewController: AVCapturePhotoCaptureDelegate  {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard let imageData = photo.fileDataRepresentation()
            else { return }
        
        self.image = UIImage(data: imageData)
        
        let storyboard = UIStoryboard(name: "Camera", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PicturePreviewViewController") as! PicturePreviewViewController
        vc.image = self.image
        present(vc, animated: true, completion: nil)
    }
}

extension ScanPhotoViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.image = pickedImage
            self.dismiss(animated: true, completion: nil)
            
            let storyboard = UIStoryboard(name: "Camera", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PicturePreviewViewController") as! PicturePreviewViewController
            vc.image = self.image
            present(vc, animated: true, completion: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
