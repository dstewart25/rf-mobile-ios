//
//  SideMenuViewController.swift
//  Related Faces
//
//  Created by Daniel Stewart on 9/20/18.
//  Copyright © 2018 Related Faces. All rights reserved.
//

import UIKit

class SideMenuViewController: UITableViewController {

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        
        switch indexPath.row {
        case 0:
            NotificationCenter.default.post(name: NSNotification.Name("ShowProfile"), object: nil)
        case 1:
            NotificationCenter.default.post(name: NSNotification.Name("ShowSettings"), object: nil)
        case 2:
            NotificationCenter.default.post(name: NSNotification.Name("ShowSignIn"), object: nil)
        default:
            break
        }
    }

}
