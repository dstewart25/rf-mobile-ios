//
//  Related_FacesTests.swift
//  Related FacesTests
//
//  Created by Daniel Stewart on 2/13/19.
//  Copyright © 2019 Related Faces. All rights reserved.
//

import XCTest
@testable import Related_Faces

class Related_FacesTests: XCTestCase {
    var helper: CognitoHelper?
    var uploadHelper: UploadHelper?
    var viewController: UIViewController?

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        helper = CognitoHelper()
        uploadHelper = UploadHelper()
        viewController = UIViewController()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        helper = nil
        uploadHelper = nil
        viewController = nil
        super.tearDown()
    }
    
    func testSignIn() {
        let _ = helper?.signOut() // Making sure the current user is signed out
        
        let expectation = XCTestExpectation(description: "Log into related faces")
        
        let username = "iostest"
        let password = "password1"
        let result = helper?.signIn(username: username, password: password, sender: viewController!)
        expectation.fulfill()
        XCTAssertEqual(LogInState.loggedIn, result)
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testErrorSignIn() {
        let _ = helper?.signOut()
        let username = "notavalidusername"
        let password = "password1"
        let result = helper?.signIn(username: username, password: password, sender: viewController!)
        XCTAssertEqual(LogInState.notLoggedIn, result)
    }
    
    func testUsernameExistsSignUp() {
        let username = "iostest"
        let password = "password1"
        let email = "rf@fmail.ooo"
        let name = "John Doe"
        let attributes = [
            "email": email,
            "name": name
        ]
        
        let result = helper?.signUp(username: username, password: password, attributes: attributes, sender: viewController!)
        XCTAssertEqual(SignUpState.unknown, result)
    }
    
    func testSignUp() {
        let username = "classexample9"
        let password = "password1"
        let email = "rf@fmail.ooo"
        let name = "John Doe"
        let attributes = [
            "email": email,
            "name": name
        ]
        
        let result = helper?.signUp(username: username, password: password, attributes: attributes, sender: viewController!)
        XCTAssertEqual(SignUpState.notConfirmed, result)
    }
    
    func testConfirmSignUp() {
        // This must be ran after the test above so that we can receive the confirmation code from an email
        let username = "classexample8"
        let password = "password1"
        let confirmationCode = "785978"
        
        let result = helper?.confirmSignUp(username: username, password: password, confirmationCode: confirmationCode, sender: viewController!)
        XCTAssertEqual(SignUpState.confirmed, result)
    }
    
    func testUpload() {
        let _ = helper?.signIn(username: "iostest", password: "password1", sender: viewController!)
        
        let image = UIImage(named: "upload_test8")
        let imageData = image?.jpegData(compressionQuality: 0.99)
        let idToken = helper?.getUserIdToken()
        
        let expectation = XCTestExpectation(description: "Upload attempt")
        
        let result = uploadHelper?.uploadImage(with: imageData!, idToken: idToken!, sender: viewController!)
        XCTAssertTrue(result!)
        expectation.fulfill()
        
        wait(for: [expectation], timeout: 30.0)
    }
    
    func testRetrieveUserDetails() {
        let _ = helper?.signIn(username: "iostest", password: "password1", sender: viewController!)
        
        let expectation = XCTestExpectation(description: "Getting user details")
        
        let attributesResult = helper?.getUserAttributes()
        let tokenResult = helper?.getUserTokens()
        expectation.fulfill()
        
        wait(for: [expectation], timeout: 5.0)
        XCTAssertTrue(attributesResult!)
        XCTAssertTrue(tokenResult!)
    }
}
